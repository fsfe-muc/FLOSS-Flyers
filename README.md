Thematische Flyer von Freier Software.
============

Jeder Flyer listet freie Software aus einem Bereich auf.

Die Auswahl richtet sich an Einsteiger & Umsteiger.

Es gibt zwei Versionen der Flyer, eine in TeX gemacht, eine in Inkscape erzeugte.
Die TeX-Version benötig noch kleinere Verbesserungen am Satz, ist dafür jedoch inhaltlich wesentlich aktueller.


Tex-Versionen
----------------------
Es existieren derzeit folgende Flyer:

* grafik  - Software aus dem Bereich Grafik
* video   - Software aus dem Bereich Video
* office  - Software aus dem Bereich Office
* edu     - Lernsoftware
* audio   - Software aus dem Bereich Audio
* web     - Software aus dem Bereich Internet & WWW
* games   - Spiele

### Kompilieren
Zum kompilieren aller Fyler einfach `make` im Ordner Tex starten.
Einzelne Flyer können mit beispielsweise `make audio` compiliert werden.

Zum Kompilieren wir luatex & fontawesome benötigt.

### Neue Flyer erstellen
* template.tex  - Vorlage für weitere Flyer
* Brainstorming - Ideensammlung für weitere Flyer

Inkscape-Versionen
----------------------
Siehe seperate README-Datei im Unterordner Inkscape.
